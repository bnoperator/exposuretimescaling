# Linreg for exposure time scaling

Integrate (replicate) measurements obtained with multiple exposure times.

# Create app and deploy

Clone the repo and build the package.

```
library(exposuretimescaling)
bntools::createApp(tags=c('Test'), mainCategory = 'Test')
bntools::deployApp()
```

# Run test


```
devtools::install_bitbucket('bnoperator/bntools')
devtools::install_bitbucket('bnoperator/bnutil')
devtools::install_bitbucket('bnoperator/bn_shiny') 


```


```
devtools::test()
```

```
bnshiny::startBNTestShiny('exposuretimescaling')
# see workspace.R for a full example
```
